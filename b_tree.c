/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   b_tree.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tony <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/08 16:21:22 by tony              #+#    #+#             */
/*   Updated: 2019/06/16 00:13:26 by tony             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <b_tree.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define __unused __attribute__((unused))

t_node  *create_node(void)
{
    t_node *node;

    node = (t_node*)malloc(sizeof(t_node));
    if (node == NULL)
        return (NULL);
    node->value = 0;
    node->right = NULL;
    node->left = NULL;
    node->root = NULL;
    return (node);
}

void    insert_node(t_node *current, t_node *next)
{
    if (current->value > next->value)
    {
        if (next->right != NULL)
            insert_node(current, next->right);
        else
        {
            current->root = next;
            next->right = current;
        }
    } 
    else 
    {
        if (next->left != NULL)
            insert_node(current, next->left);
        else
        {
            current->root = next;
            next->left = current;
        }
    }
}

void    insert(t_node **tree, int value)
{
    t_node *current;

    current = create_node();
    current->value = value;
    if (current == NULL)
        return; 
    if (*tree == NULL)
    {
        *tree = current;
        (*tree)->root = current;
    }
    else
        insert_node(current, (*tree)->root);
}

t_node  *search(t_node *root, int value)
{
    if (root != NULL)
    {
        if (value > root->value)
            return search(root->right, value);
        else if (value < root->value)
            return search(root->left, value);
        else
            return root;
    }
    else
        return (NULL);
}

t_node  *tree_min(t_node *root)
{
    if (root->left != NULL)
        return tree_min(root->left);
    else
        return root;
}

void    delete_leaf(t_node *parent, t_node *current)
{
    if (parent != NULL && current != NULL)
    {
        if (current->value > parent->value)
            parent->right = NULL;
        else 
            parent->left = NULL;
        free(current);
    }
}

void    delete_node_has_children(t_node *current)
{
    t_node *min;

    min = tree_min(current->right);
    current->value = min->value;
    delete_leaf(min->root, min);
}

void    delete_node_has_child(t_node *parent, t_node *current)
{
    t_node  *tmp_child;
    t_node  *tmp_parent;
    const int value = current->value;

    if (current->right != NULL)
        tmp_child = current->right;
    else
        tmp_child = current->left;

    tmp_parent = parent;
    delete_leaf(parent, current);

    if (value > tmp_parent->value)
        tmp_parent->right = tmp_child;
    else
        tmp_parent->left = tmp_child;
}

/*
 *                      DELETING A NODE FROM A BST
 * 
 * THERE ARE 3 MAIN CASES TO DELETE A NODE :
 *
 * - CASE 1: 
 *      THE NODE HAS NO CHILDREN
 *      - DELETE THE REFERENCE FROM THE PARENT NODE AND FREE THE NODE
 * - CASE 2:
 *      THE NODE HAS 1 CHILDREN
 *      - DELETE THE REFERENCE FROM THE PARENT NODE AND FREE THE NODE
 *      - REPLACE THE OLD REFERENCE WITH THE CHILDREN NODE REFERENCE
 * - CASE 3:
 *      THE NODE HAS 2 CHILD
 *      - FIND THE MINIMUM OF THE RIGHT SUBTREE
 *      - REPLACE THE VALUE OF THE MINIMUM SUBTREE WITH THE NODE TO DELETE
 *      - DELETE THE MINIMUM NODE OF THE RIGHT SUBTREE
 **/

void    delete(t_node *root, int value)
{
    t_node *found;

    found = search(root, value);
    if (found != NULL)
    {
        if (found->right != NULL && found->left != NULL)
            delete_node_has_children(found);
        else if (found->right != NULL || found->left != NULL)
            delete_node_has_child(found->root, found);
        else
            delete_leaf(found->root, found);
    }
}

int     main(void)
{
    t_node *root;

    root = NULL;
    insert(&root, 50); 
    insert(&root, 30); 
    insert(&root, 20); 
    insert(&root, 40); 
    insert(&root, 45); 
    insert(&root, 70); 
    insert(&root, 60); 
    insert(&root, 80); 
    delete(root, 50);

    printf("The root value is %i\n", root->value);
    return (0);
}
