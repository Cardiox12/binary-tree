# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: tony <marvin@42.fr>                        +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/06/12 12:54:25 by tony              #+#    #+#              #
#    Updated: 2019/06/12 16:26:24 by toto             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC 			= gcc
FLAGS 		= -g -Wall -Werror -Wextra 
NAME		= binary_tree
INCLUDE 	= include
.PHONY 		= all clean re
DEBUGGER	= lldb 

all:
	$(CC) -o $(NAME)  b_tree.c $(FLAGS) -I $(INCLUDE)

clean:
	rm -rf $(NAME) $(NAME).dSYM

debug: all
	$(DEBUGGER) $(NAME)

segdeb: all
	$(DEBUGGER) -- $(NAME)

re: clean all
